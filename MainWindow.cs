﻿using System;
using Gtk;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Json;

public partial class MainWindow : Gtk.Window
{


    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    protected void addNum(object sender, EventArgs e)
    {
        double Num_1 = Double.Parse(num1.Text);
        double Num_2 = Double.Parse(num2.Text);
        lblTotal.Text = (Num_1 + Num_2).ToString();
    }

    protected void subNum(object sender, EventArgs e)
    {
        double Num_1 = Double.Parse(num1.Text);
        double Num_2 = Double.Parse(num2.Text);
        lblTotal.Text = (Num_1 - Num_2).ToString();
    }

    protected void mulNum(object sender, EventArgs e)
    {
        double Num_1 = Double.Parse(num1.Text);
        double Num_2 = Double.Parse(num2.Text);
        lblTotal.Text = (Num_1 * Num_2).ToString();
    }

    protected void divNum(object sender, EventArgs e)
    {
        double Num_1 = Double.Parse(num1.Text);
        double Num_2 = Double.Parse(num2.Text);
        lblTotal.Text = (Num_1 / Num_2).ToString();
    }
}
